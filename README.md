# Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!

Workers of the world, unite!

Пролетарии всех стран, соединяйтесь!

# Основная информация

Это репозиторий библиотеки документации Реального виртуального мира Универсо (Universo).

Смотрите список активных Обсуждений (Issues) здесь в репозитории https://gitlab.com/tehnokom/universo-dokumentaro/-/issues

Более подробная информация в главном репозитории Универсо вот тут по ссылке https://gitlab.com/tehnokom/universo

Наш дискорд-сервер по Универсо https://discord.gg/bsvdPy5

# Документация по Универсо

[Этот проект на ReadTheDocs](https://dok.universo.pro)

ReadTheDocs сам компилирует проект при изменении.

Для полноценной работы над проектом понадобится его клонировать на свой компьютер и установить рабочие утилиты.

**Установка pip3**

Сначала убедимся, что Python 3 установлен в системе:

`python3 --version`

Команда выводит текущую версию Python, которая используется в системе. Теперь установим нужную версию PIP:

`sudo apt install python3-pip`

И смотрим информацию об установленной утилите:

`pip3 --version`

**Устанавливаем Sphinx Documentation**

`pip3 install -U Sphinx`

Устанавливаем тему ReadTheDocs

`pip3 install sphinx_rtd_theme`

Переходим в папку с клонированным проектом. Например

`cd universo-dokumentoj`

Переходим на рабочую ветку (например develop)

`git checkout develop`

Для того, чтоб скомпилировать проект, в рабочей папке выполняем команду

`make html`

Редактируемые файлы находятся в папке `source` (с расширением `.rst`).

Скомпилированные файлы проекта находятся в папке `build`.

[Документация по Sphinx Documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)



*Пролетарии всех стран, соединяйтесь!*
*Proletoj el ĉiuj landoj, unuiĝu!*