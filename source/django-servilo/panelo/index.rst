.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/

Административная панель Django
==============================

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. image:: _static/image_1.png 

- :doc:`Objektoj de Universo <objektoj>`
- :doc:`Resursoj de Universo <resursoj>`
