.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/


Объекты в Universo
===================

.. note:: Этот материал находится в состоянии наполнения информацией.

Раздел объектов в Административной панели Django выглядит вот так:

.. image:: _static/image_12.png 

Связи объектов
--------------

:guilabel:`Ligiloj de objektoj de Universo` - это связи объектов.

.. image:: _static/image_31.png 

При создании новой связи между объектами с типом "Связь с персоналией", заполняется следующий перечень полей:

.. image:: _static/image_32.png 

При создании новой связи между объектами с типом "Связь с родителем", заполняется следующий перечень полей:

.. image:: _static/image_33.png 

Объекты
-------

:guilabel:`Objektoj de Universo` - это справочник объектов.

.. image:: _static/image_13.png 

Большая часть всех объектов создается автоматически, на основе шаблонов объектов. При создании нового шаблона объекта, заполняется следующий перечень полей:

.. image:: _static/image_14.png 

.. image:: _static/image_15.png 

При необходимости создать объект, в ручном режиме, а не автоматически, заполняются все те же поля кроме полей системного шаблона.


Владельцы объектов
------------------

:guilabel:`Posedantoj de objektoj de Universo` - это владельцы объектов.

.. image:: _static/image_40.png 

При создании новых владельцев объектов, заполняется следующий перечень полей:

.. image:: _static/image_41.png 

Статусы владельцев объектов
----------------------------

:guilabel:`Statusoj de posedantoj de objektoj de Universo` - это статусы владельцев объектов.

.. image:: _static/image_42.png 

При создании новых статусов владельцев объектов, заполняется следующий перечень полей:

.. image:: _static/image_43.png 

Склады объектов
---------------

:guilabel:`Stokejoj de objektoj de Universo` - это склады объектов.

.. image:: _static/image_34.png 

При создании новых типов складов объектов, заполняется следующий перечень полей:

.. image:: _static/image_35.png 

Типы связей объектов
--------------------

:guilabel:`Tipoj de ligiloj de objektoj de Universo` -  это типы связей объектов.

.. image:: _static/image_36.png 

При создании новых типов связей объектов, заполняется следующий перечень полей:

.. image:: _static/image_37.png 

Типы владельцев объектов
------------------------

:guilabel:`Tipoj de posedantoj de objektoj de Universo` -  это типы владельцев объектов.

.. image:: _static/image_44.png

При создании новых типов владельцев объектов, заполняется следующий перечень полей:

.. image:: _static/image_45.png

Пользователи объектов
----------------------

:guilabel:`Uzantoj de objektoj de Universo` - это управляемый объект пользователя, то есть каким объектом управляет непосредственно сам пользователь.

.. image:: _static/image_46.png

При создании новых пользователей объектов, заполняется следующий перечень полей:

.. image:: _static/image_47.png

Шаблоны объектов
----------------

:guilabel:`Ŝablono de objektoj de Universo` - это шаблоны объектов, устаревший функционал, будет удален.

.. image:: _static/image_38.png 

При создании новых шаблонов объектов, заполняется следующий перечень полей:

.. image:: _static/image_39.png 
