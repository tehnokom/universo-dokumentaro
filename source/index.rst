.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/

Добро пожаловать в Универсо!
===============================================


.. note::

   Этот материал находится в состоянии наполнения информацией.


Добро пожаловать на официальную документацию `Реального виртуально мира Универсо <https://universo.pro>`_ (RVM Universo) - третьего блока Всемирной автоматизированной системы Техноком, бесплатной и открытой платформы, поддерживаемой сообществом! Если вы здесь впервые, мы рекомендуем вам прочесть Введение, чтобы иметь представление о том, что данная документация из себя представляет.

Если вы хотите присоединиться к созданию и развитию RVM Universo, на странице ":doc:`Для новых участников <universo/konstrukciisto/por-novaj-komplicoj>`" даны базовые ответы на все вопросы. В том числе о прошлом, настоящем и будущем RVM Universo, о том какая от него общественная и личная польза, как присоединиться и чем можно помочь, какой экономический план проекта и так далее.

Список разделов внизу страницы и боковая панель позволят вам с лёгкостью получить доступ к любой интересующей вас теме в документации. Вы так же можете воспользоваться функцией поиска в левом верхнем углу.

Мы рекомендуем начать ознакомление с :doc:`основной информации <universo/baza/index>` об RVM Universo.
  
.. toctree::
   :maxdepth: 1
   :caption: Универсо
   :name: sec-cxefa-universo

   universo/baza/index
   universo/uzanto/index
   universo/partnero/index
   universo/konstrukciisto/index
   universo/testado/index
   universo/dokumentaro/index
   

.. toctree::
   :maxdepth: 1
   :caption: Клиентское приложение
   :name: sec-cxefa-client

   client/index
   client/ekzemplo
   client/servila-interago
   client/klienta-apliko
   client/strukturo-klienta-apliko

.. toctree::
   :maxdepth: 1
   :caption: Веб-клиент
   :name: sec-cxefa-web-client

   web-client/index
      
.. toctree::
   :maxdepth: 1
   :caption: Мобильное приложение
   :name: sec-cxefa-mob-client

   mob-client/index

.. toctree::
   :maxdepth: 1
   :caption: Django-сервер
   :name: sec-cxefa-django-server

   django-servilo/index
   
.. toctree::
   :maxdepth: 1
   :caption: Godot-сервер
   :name: sec-cxefa-godot-server

   godot-servilo/index
   godot-servilo/strukturo-servilejo-apendico

Индексы и таблицы
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
