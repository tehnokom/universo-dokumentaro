..    Пролетарии всех стран, соединяйтесь!
      Proletoj el ĉiuj landoj, unuiĝu!
.. vim: syntax=rst
.. _contrib:
.. highlight:: bash

Основа клиент серверного взаимодействия (клиент)
================================================

.. note:: Этот материал находится в состоянии наполнения информацией.

В рамках создания основы клиент-серверной архитектуры был создан `сервер <https://gitlab.com/tehnokom/universo-servilo>`_. Его описание см. по `ссылке <https://gitlab.com/tehnokom/universo-servilo/-/wikis/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D0%B0-%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82-%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%BD%D0%BE%D0%B3%D0%BE-%D0%B2%D0%B7%D0%B0%D0%B8%D0%BC%D0%BE%D0%B4%D0%B5%D0%B9%D1%81%D1%82%D0%B2%D0%B8%D1%8F-(%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80)>`_.
В клиентской части были добавлены скрипты реализующие подключение к серверу и получение информации (сообщений) от него.
Функционал подключения к серверу размещён в синглтоне (`скрипт <https://gitlab.com/tehnokom/universo/-/blob/develop/blokoj/network/scriptoj/network.gd>`_ подключенный в автозагрузку):

.. image:: _static/screen01.png
.. image:: _static/screen02.png

Там прописан ip-адрес и порт сервера, реализованы функции для подключения к серверу и функции для удалённого вызова сервером.

Если сервер запущен на локальной машине, необходимо в начале данного скрипта присвоить константе ip значение "127.0.0.1"

Само подключение к серверу инициализируется после `сцены авторизации <https://gitlab.com/tehnokom/universo/-/blob/develop/blokoj/rajtigo/auth_menu.tscn>`_ в `скрипте <https://gitlab.com/tehnokom/universo/-/blob/develop/blokoj/kosmostacio/Kosmostacio.gd>`_ :

.. image:: _static/screen03.png

Вывод сообщений реализован в `скрипте <https://gitlab.com/tehnokom/universo/-/blob/develop/blokoj/title_screen/Menu/Scripts/server.gd>`_ сцены, отвечающей за взаимодействие с сервером:

.. image:: _static/screen04.png












