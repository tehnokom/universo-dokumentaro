..    Пролетарии всех стран, соединяйтесь!
      Proletoj el ĉiuj landoj, unuiĝu!
.. vim: syntax=rst
.. _contrib:
.. highlight:: bash

Все инструкции даны для Ubuntu Linux.


Настройка окружения.
====================

.. note:: Этот материал находится в состоянии наполнения информацией.

Следуя `мануалу <https://docs.godotengine.org/en/latest/tutorials/plugins/gdnative/gdnative-cpp-example.html>`_ настраиваем:

* Исполняемый файл Godot 3.x,
* C++ компилятор,
* SCons в качестве сборочного инструментария,
* Копия godot-cpp репозитория.


Исполняемый файл Godot 3.x
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Скачиваем Godot 3.2 со `страницы <https://godotengine.org/download/linux>`_

Для удобства запуска Godot из командной строки, добавляем alias:

::

       alias godot="Путь_к_исполняемому_файлу_годо/Godot_v3.2-stable_x11.64"


Добавляем эту команду в файл инициализации bash:

::

      echo "alias godot=\"Путь_к_исполняемому_файлу_годо/Godot_v3.2-stable_x11.64\"" >> .bashrc



C++ компилятор
~~~~~~~~~~~~~~~

Установка (инструменты разработчика):

::

      sudo apt install build-essential



SCons
~~~~~~

Установка:

::

      sudo apt install scons



Получение копии godot-cpp репозитория
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Выбираем (создаём) директорию для исходников godot-cpp:

::

      mkdir gdnative_cpp
      cd gdnative_cpp


Клонируем необходимые репозитории:

::

      git init
      git submodule add https://github.com/GodotNativeTools/godot-cpp
      cd godot-cpp
      git submodule update --init


Сборка C++ биндингов
--------------------

В директории gdnative_cpp енерируем api.json:

::

      godot --gdnative-generate-json-api api.json


Генерируем и собираем биндинги:

::

      cd godot-cpp
      scons platform=linux generate_bindings=yes use_custom_api_file=yes custom_api_file=../api.json
      cd ..



Настройка редактора vscode
---------------------------

1. Устанавливаем C/C++ плагин от Microsoft:

.. image:: _static/01.png 


Клонирование репозитория Universo
---------------------------------

**Внимание, ссылка как пример дана на основной репозиторий, но работать нужно со своими форками.**

В директории gdnative выполняем:

::

      git clone https://gitlab.com/tehnokom/universo.git


Переходим в директорию с C++ модулем и выполняем scons:

::

      cd universo/blokoj/toroid/skriptoj/cpp
      scons platform=linux














