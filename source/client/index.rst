.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/!

Клиентское приложение
=====================

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. toctree::
   :maxdepth: 1
   :caption: Клиент:
   
   about/index

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
