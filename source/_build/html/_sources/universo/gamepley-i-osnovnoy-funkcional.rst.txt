..    Пролетарии всех стран, соединяйтесь!
      Proletoj el ĉiuj landoj, unuiĝu!
.. vim: syntax=rst
.. _contrib:
.. highlight:: bash

Геймплей и основной функционал
=========================================

Основная информация
~~~~~~~~~~~~~~~~~~~

В рамках Реального виртуального мира Универсо создаётся большой перечень различного функционала и игрового геймплея.

Функционал реального мира
~~~~~~~~~~~~~~~~~~~~~~~~~

В рамках реального мира первичный реализуемый функционал Универсо будет направлен на создание возможности для индивидуальной и совместной работы над проектами / задачами, получения знаний и т.д. Позже по мере реализации этот вопрос будет описан более подробнее.

Геймплей виртуальных миров
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Игровое взаимодействие будет возможно как в одиночном режиме, так и в кооперативных режимах, в том числе в сложных кооперативных режимах. Геймплей разнообразный и имеет отличия в разных параллельных мирах и в разных локациях, например, в космической локации и разных планетарных локациях.

Основной перечень игровых возможностей для космической локации:

- Выполнение миссий и исследование.
- Добыча руды и газа.
- Грузоперевозки.
- Производство.
- Торговля.
- Создание и развитие организаций.
- Сражения.

Принципы управления объектами виртуального космоса
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Пользователь Универсо в рамках коммунистического и капиталистического параллельных миров будет управлять разными объектами, например, роботами-аватарами, космическими кораблями и так далее. Ниже описание основных принципов / подходов в управлении объектами.

1. Управление осуществляется в разных случаях от третьего и от первого лица. Часто будет возможность переключаться между этими режимами. Нужно помнить, что виртуальный космос в Универсо виртуален только в представлении в нашем параллельном мире, в тех параллельных мирах где идёт реальное действие, виртуальный космос - реален. Поэтому управление видом от третьего лица осуществляется не каким-то магическим / божественным образом, а за счёт технических возможностей, благородя применению специальных зондов от пользователя и общих станций обзора, которые сделаны по специальным технологиям и имеют многократное дублирование, благородя чему получение изображения с них обычно стабильно, но могут быть ситуации, при которых могут возникнуть некоторые помехи в передаче изображения.

2. Управление большинством объектов за счёт их высокой автоматизации возможно одним пользователем. Будут объекты, например, крупные космические корабли, в которых будут варианты управления как одним пользователем, но с меньшей эффективностью по различным показателям, так и несколькими пользователями в кооперативном режиме - с более высокой эффективность по этим показателям. А также будут объекты, например, крупнейшие космические станции, крупнейшие космические корабли, которыми возможно будет управлять только в кооперативном режиме.

3. Будет возможность переключаться на некоторые развитые планетарные локации, при помощи получения на них робота-субаватара. В этих локациях сначала пользователю будет даваться так же базовый робот-субаватар, но в разных локациях будут во многом реализовываться различные сценарии, поэтому будут разные объекты, в том числе разные базовые роботы-субаватары. Для переключения между роботами-аватарами и роботами-субаватарами, не нужно будет получать каких-то специальных навыков и это будет происходить мгновенно, то есть не нужно будет куда-то лететь.

4. Возможность / эффективность управления различными объектами будет зависеть от полученных / прокаченных навыков у робота-аватара (субаватара) пользователя, а так же у ИИ различных объектов. То есть прокачивать некоторые навыки нужно также, например, у космических кораблей, космических станций и т.д. Но по мере развития Универсо, так же будут реализованы механизмы по которым влиять на виртуальный мир, будут в различной мере полученные / подтвержденные реальные навыки пользователя Универсо.

5. Кооперативный режим управления управления подразумевает, что у различных объектов будут роли, которые могут взять на себя отдельные пользователи (через своих роботов-аватаров), например, пилот, техник, оператор оружия и т.д. Они смогут исполнять эти роли если у их роботов-аватаров есть необходимые навыки и чем более лучше развиты необходимые навыки тем более эффективнее будет действовать пользователь на занимаемой им роли. В зависимости от размера объекта, например размера космической станции или космического корабля, ролей может быть меньше или больше. Чтобы постоянно функционировали крупнейшие объекты, необходимо будет чтобы некоторые основные роли были заняты постоянно, то есть чтобы пользователи занимали их в несколько смен.

6. Каким именно объектом непосредственно управляет пользователь через основную систему управления, будет зависеть от различных обстоятельств и алгоритмов. В начале, при первом подключение к виртуальному космосу Универсо, пользователь будет получать базового робота-аватара находящегося в базовом корабле, который находится в ангаре космической станции. Объектом управления будет этот базовый космический корабль. Если пользователь переключится на своего робота-аватара, при этом переведя свой корабль на автопилот, то он сможет управлять своим роботом-аватаром, ходить им по кораблю, а если будет находится на космической станции, то выйти из корабля на станцию. Так же пользователь, если у него есть право использования разных кораблей, сможет пересаживаться своим роботом-аватаром на с одного на другой корабль.

7. Некоторые объекты могут быть запрограммированы пользователем через функционал задач объекта на работу в автоматическом режиме. Длительность работы в автоматическом режиме, возможные места и максимальное расстояние работы в автоматическом режиме от робота-аватара (субаватара) пользователя, а так же эффективность автономной работы объекта будут зависеть от получения навыков робота-аватара и объекта работающего в автономном режиме и уровня этих навыков. 

8. Пользователь может иметь различные ограничения по управлению объектами. Эти ограничения будут, например, по продолжительности использования, района использования и т.д. Зависеть это будет от того какие у пользователя права на объект. Например, права использования объекта может быть предоставлено организацией или объект может быть в аренде. Дополнительно про права, можно прочитать в статье `"Права на владение объектами" <Права-на-владение-объектами>`_.

.. note:: Этот материал находится в состоянии наполнения информацией.
