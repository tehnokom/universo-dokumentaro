.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/!

О документации
===============

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. toctree::
   :maxdepth: 1
   :name: sec-dokumentaro

   enkonduko
   jxurnalo
   farado-dokumentaroj

