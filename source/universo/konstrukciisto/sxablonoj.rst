..    Пролетарии всех стран, соединяйтесь!
      Proletoj el ĉiuj landoj, unuiĝu!
.. vim: syntax=rst
.. _contrib:
.. highlight:: bash

Система шаблонов
================

.. note:: Этот материал находится в состоянии наполнения информацией.

Основная информация
~~~~~~~~~~~~~~~~~~~

Шаблоны применяются для создании на их основе новых сущностей. Шаблоны можно разделить на две основных группы, это различные системные шаблоны и пользовательские шаблоны. Сейчас речь в первую очередь о системных шаблонах на основе, которых создаются различные новые сущности.

В рамках джанго-бэкенда изначально начали создаваться отдельные модели для шаблонов, но по мере развития системны шаблонов стало видно что придётся создать слишком много дополнительных моделей и на текущий период развития, лучше / проще будет создать в нужных моделях необходимые маркеры при помощи которых помечать записи-шаблоны и для них отдельное поле с цифровыми идентификаторами шаблона. А для того, чтобы активировать применение шаблонов по различным алгоритмам используется функционал сигналов джанго.

Логика работы
~~~~~~~~~~~~~~

На первых этапах реализации системы шаблонов, много логики было реализовано именно при помощи кода в джанго сигналах. В том числе там прописывались параметры и каких у них должны быть значения. В рамках доработки системы использования шаблонов, предлагается реализовать логику, по которой новые сущности в таблицах будут создаваться за счёт копирования ранее созданных сущностей помеченных как шаблоны с копированием значений большинства полей с заменой только некоторых данных.

Общая концепция копирования / создания на основе шаблона выглядит вот так:

- Скопировать в тоже место, ну то есть новая запись будет создаваться в той же модели / таблице где и существует текущий шаблон.
- С новым UUID.
- С новой датой.
- С новым владельцем.
- Без пометки что это шаблон и без ID шаблона.
- Копировать так же все найденные связи, кроме связей которые входят в перечень связей исключений, которые копировать не нужно. Вообще базовая логика будет такая, что будет прописано копировать именно те связи где стоит параметр, что это шаблон.





