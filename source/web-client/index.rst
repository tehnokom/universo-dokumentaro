.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/

Структура Веб-клиента RVM Universo
==================================

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. toctree::
   :maxdepth: 1
   :name: sec-web-client

   strukturo/index

