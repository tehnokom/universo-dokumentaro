.. Grave! Important! Важно!

.. Proletoj el ĉiuj landoj, unuiĝu!
   Workers of the world, unite!
   Пролетарии всех стран, соединяйтесь!

.. https://universo.pro/!

Оглавление структуры Веб-клиента
================================

.. note::

   Этот материал находится в состоянии наполнения информацией.

.. toctree::
   :maxdepth: 1
   :name: sec-web-client-strukturo

   struktura
   nuxt
   assets
   componets
   layouts 
   middleware
   modules
   node_modules
   pages
   plugins
   static
   store
   eslintrc
   gitignore
   nuxt-config-js
   package-lock-json
   package-json
   readme-md
   urls-js
   utils-js
   retejo

   
